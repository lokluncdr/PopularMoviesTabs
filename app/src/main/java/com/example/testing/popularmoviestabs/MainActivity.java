package com.example.testing.popularmoviestabs;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.testing.popularmoviestabs.adapters.SectionsPagerAdapter;
import com.example.testing.popularmoviestabs.di.component.ControllerComponent;
import com.example.testing.popularmoviestabs.di.component.DaggerControllerComponent;
import com.example.testing.popularmoviestabs.di.module.AdapterModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.testing.popularmoviestabs.R.layout.activity_main;


public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    @Inject
    SectionsPagerAdapter mSectionsPagerAdapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    ViewPager mViewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.movie_list_ordinary_textview)
    TextView ordinaryTextView;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ControllerComponent controllerComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);

        //Make SectionsPagerAdapter object available for injection in this class
        getActivityComponent().inject(this);

        //Bind Views
        ButterKnife.bind(this);

        //Set up Toolbar
        setSupportActionBar(toolbar);

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        //Keep all Fragments
        mViewPager.setOffscreenPageLimit(2);

        //Set up Tabs
        tabLayout.setupWithViewPager(mViewPager);
    }

    public ControllerComponent getActivityComponent() {
        if (controllerComponent == null) {
            controllerComponent = DaggerControllerComponent
                    .builder()
                    .adapterModule(new AdapterModule(this))
                    .build();
        }
        return controllerComponent;
    }
}