package com.example.testing.popularmoviestabs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.testing.popularmoviestabs.di.component.ControllerComponent;
import com.example.testing.popularmoviestabs.di.component.DaggerControllerComponent;
import com.example.testing.popularmoviestabs.di.module.ControllerModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An activity representing a single Movie detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a .
 */
public class MovieDetailActivity extends AppCompatActivity {
    //FIELDS
    private final String FRAGMENT_TAG = "popular_movies_portrait";
    public ControllerComponent controllerComponent;
    @BindView(R.id.detail_toolbar)
    Toolbar toolbar;
    @Inject
    MovieDetailFragment fragment;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        getControllerComponent().inject(this);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        if (savedInstanceState == null)//Keep this, for when in portrait mode and user rotates screen
        {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            fragment.setArguments(savedInstanceState);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movie_detail_container, fragment, FRAGMENT_TAG)
                    .commit();
        }
    }

    private ControllerComponent getControllerComponent() {
        controllerComponent = DaggerControllerComponent
                .builder()
                .controllerModule(new ControllerModule())
                .build();

        return controllerComponent;
    }
}
