package com.example.testing.popularmoviestabs;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testing.popularmoviestabs.adapters.ReviewAdapter;
import com.example.testing.popularmoviestabs.adapters.TrailerAdapter;
import com.example.testing.popularmoviestabs.di.component.ControllerComponent;
import com.example.testing.popularmoviestabs.di.component.DaggerControllerComponent;
import com.example.testing.popularmoviestabs.di.module.AdapterModule;
import com.example.testing.popularmoviestabs.di.module.ControllerModule;
import com.example.testing.popularmoviestabs.di.module.NetworkModule;
import com.example.testing.popularmoviestabs.model.MovieReviewsResponse;
import com.example.testing.popularmoviestabs.model.MovieTrailersResponse;
import com.example.testing.popularmoviestabs.pojo.Movie;
import com.example.testing.popularmoviestabs.pojo.MovieReview;
import com.example.testing.popularmoviestabs.pojo.MovieTrailer;
import com.example.testing.popularmoviestabs.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A fragment representing a single Movie detail screen.
 * This fragment is either contained in a
 * in two-pane mode (on tablets) or a {@link MovieDetailActivity}
 * on handsets.
 */
public class MovieDetailFragment extends Fragment {
    //FIELDS
    private static final String API_KEY = BuildConfig.POPULAR_MOVIES_API_KEY;
    private final String FRAGMENT_TAG_PORTRAIT = "popular_movies_portrait";
    private final String FRAGMENT_TAG_LANDSCAPE = "popular_movies_landscape";
    private final String MOVIE = "Movie";
    private final String _TRAILER = "trailer";
    private final String _REVIEW = "review";
    private final String REMOVE_FRAGMENT = "remove_fragment";
    private final String SCROLLVIEW_POSITION = "scrollview_position";
    private final String SCROLLVIEW_HEIGHT = "scrollview_height";
    @BindView(R.id.movie_detail_poster)
    ImageView movieImageView;
    @BindView(R.id.movie_detail_release_date)
    TextView txtView_ReleaseDate;
    @BindView(R.id.movie_detail_vote_average)
    TextView txtView_VoteAverage;
    @BindView(R.id.movie_detail_overview)
    TextView txtView_Overview;
    @BindView(R.id.movie_detail_favourite)
    ImageView favouriteImageView;
    @BindView(R.id.listview_trailer)
    ListView movieTrailerListView;
    @BindView(R.id.listview_review)
    ListView movieReviewListView;
    @Inject
    List<MovieTrailer> movieTrailerList;
    @Inject
    List<MovieReview> movieReviewsList;
    @Inject
    Bundle bundle;
    @Inject
    ApiInterface apiInterface;
    private FragmentActivity fragmentActivity;
    private Context context;
    private boolean mReview = false;
    private boolean mTrailer = false;
    private int mHeightOfListViews;
    private boolean mTwoPane;
    private boolean removeFragment;
    private int[] mOldScrollPositionArray;
    private int mOldScrollViewHeight;
    private double newScrollPosition;
    private NestedScrollView mScrollView;
    private Movie movie;
    private Unbinder unbinder;
    private CompositeDisposable compositeDisposable;
    private ControllerComponent controllerComponent;

    //CONSTRUCTOR
    //Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon screen orientation changes).
    public MovieDetailFragment() {
    }

    //METHODS
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentActivity = getActivity();

        //Set up Dagger2
        getActivityComponent().inject(this);

        compositeDisposable = new CompositeDisposable();

        //For saving and setting scrollview position
        mOldScrollPositionArray = new int[2];

        MovieDetailFragment fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_PORTRAIT);
        //When in portrait mode set twopane to false, otherwise if in landscape mode set it to true
        if (fragment != null) {
            mTwoPane = false;

            //For saving and setting scrollview position
            mScrollView = (NestedScrollView) getActivity().findViewById(R.id.movie_detail_container);
        } else {
            fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_LANDSCAPE);

            if (fragment != null) {
                mTwoPane = true;

            }
            //After switching to portrait after being in landscape
            //Check what has been saved in removeFragment in onSaveInstanceState
            if (savedInstanceState != null) {
                removeFragment = savedInstanceState.getBoolean(REMOVE_FRAGMENT);
                //Remove this current fragment
                if (mTwoPane && removeFragment) {
                    fragmentActivity.getSupportFragmentManager().beginTransaction()
                            .remove(fragment)
                            .commit();
                }
            }
        }

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            setHasOptionsMenu(true);

            context = getContext();

            //Retrieve Movie for Detail
            bundle = fragmentActivity.getIntent().getBundleExtra(MOVIE);
            if (bundle == null) {
                bundle = getArguments();
            }
            movie = bundle.getParcelable(MOVIE);

            //Get Trailers & Reviews for this movie
            if (Utility.isNetworkAvailable(context)) {
                //Retrieve movietrailers with RetroFit, which is retrieved through an Observable (stream)
                //It's also added to a CompositeDisposable so that resources will be freed in onPause()
                compositeDisposable.add(apiInterface.getMovieTrailers(movie.getId(), API_KEY)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new DisposableObserver<MovieTrailersResponse>() {

                            @Override
                            public void onNext(MovieTrailersResponse movieTrailers) {
                                movieTrailerList = movieTrailers.getResults();

                                //Send list to listview
                                addTrailers(movieTrailerListView, movieTrailerList);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onComplete() {

                            }
                        }));

                //Retrieve moviereviews with RetroFit, which is retrieved through an Observable (stream)
                //It's also added to a CompositeDisposable so that resources will be freed in onPause()
                compositeDisposable.add(apiInterface.getMovieReviews(movie.getId(), API_KEY)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new DisposableObserver<MovieReviewsResponse>() {

                            @Override
                            public void onNext(MovieReviewsResponse movieReviews) {
                                movieReviewsList = movieReviews.getResults();

                                //Send list to listview
                                addReviews(movieReviewListView, movieReviewsList);
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onComplete() {

                            }
                        }));

            } else {
                Toast.makeText(fragmentActivity, "There's no internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public ControllerComponent getActivityComponent() {
        if (controllerComponent == null) {
            controllerComponent = DaggerControllerComponent
                    .builder()
                    .controllerModule(new ControllerModule())
                    .networkModule(new NetworkModule())
                    .adapterModule(new AdapterModule())
                    .build();
        }
        return controllerComponent;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            //When in landscape mode. With "Master/Detail" view and user switches back to portrait mode, don't execute the code below
            //Retrieve Movie for title
            bundle = fragmentActivity.getIntent().getBundleExtra(MOVIE);
            if (bundle == null) {
                bundle = getArguments();
                movie = bundle.getParcelable(MOVIE);
            }

            //Set title
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) fragmentActivity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(movie.getTitle());
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save viewstate for title when user rotates screen
        outState.putParcelable(MOVIE, movie);

        if (!mTwoPane) { //Enter when in portrait mode
            //Save scroll position as well
            outState.putIntArray(SCROLLVIEW_POSITION, new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
            outState.putInt(SCROLLVIEW_HEIGHT, mScrollView.getChildAt(0).getMeasuredHeight());

        }
        MovieDetailFragment fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_PORTRAIT);
        //Only remove when in master/detail flow
        //NO PORTRAIT MODE exists
        if (fragment == null) {
            fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_LANDSCAPE);
            //LANDSCAPE MODE exists
            if (fragment != null) {
                removeFragment = true;
                outState.putBoolean(REMOVE_FRAGMENT, removeFragment);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        fragmentActivity.getMenuInflater().inflate(R.menu.detail_menu, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.action_share);

        // Fetch and store ShareActionProvider
        ShareActionProvider mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        //Set up the intent
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

        // Add data to the intent, the receiving app will decide what to do with it.
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.shareintent_subject));

        String trailerURL;
        if (movieTrailerList == null || movieTrailerList.size() == 0) {
            trailerURL = "of " + movie.getTitle();
        } else {
            trailerURL = "https://www.youtube.com/watch?v=" + movieTrailerList.get(0).getKey();
        }

        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.shareintent_text) + " " + trailerURL);
        mShareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.movie_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            //Set Image
            Picasso.with(context).load("http://image.tmdb.org/t/p/w342/" + movie.getPoster_path()).into(movieImageView);

            //Set Release Date, Vote Average and Overview
            txtView_ReleaseDate.setText(movie.getRelease_date());
            txtView_VoteAverage.setText(Utility.roundOff(movie.getVote_average()));
            txtView_Overview.setText(movie.getOverview());

            //Set Favourite
            if (Utility.isInDatabase(context, movie)) {
                favouriteImageView.setImageResource(R.mipmap.ic_star_black_48dp);
                favouriteImageView.setTag(R.mipmap.ic_star_black_48dp);
            } else {
                favouriteImageView.setImageResource(R.mipmap.ic_star_border_black_48dp);
                favouriteImageView.setTag(R.mipmap.ic_star_border_black_48dp);
            }
            //Mark as favouriteImageView
            favouriteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (favouriteImageView.getTag().equals(R.mipmap.ic_star_border_black_48dp)) {
                        //Show that user clicked on item
                        Animation animation = new AlphaAnimation(0.3f, 1.0f);
                        animation.setDuration(200);
                        v.startAnimation(animation);

                        favouriteImageView.setImageResource(R.mipmap.ic_star_black_48dp);
                        favouriteImageView.setTag(R.mipmap.ic_star_black_48dp);
                        //Save Favourite to Shared Preferences
                        Utility.saveToDatabase(context, movie);
                    } else {
                        favouriteImageView.setImageResource(R.mipmap.ic_star_border_black_48dp);
                        favouriteImageView.setTag(R.mipmap.ic_star_border_black_48dp);
                        //Remove Favourite from Shared Preference
                        Utility.removeFromDatabase(context, movie);
                    }
                }
            });

            //Retrieve listview position
            if (savedInstanceState != null && !mTwoPane) { //Enter when in Portrait mode
                mOldScrollPositionArray = savedInstanceState.getIntArray(SCROLLVIEW_POSITION);
                mOldScrollViewHeight = savedInstanceState.getInt(SCROLLVIEW_HEIGHT);
            }
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void addTrailers(ListView trailersListView, List<MovieTrailer> movieTrailerList) {
        if (movieTrailerList.isEmpty()) {
            //Let user know there are no trailers, by appropriating the list with an empty movie
            movieTrailerList.add(new MovieTrailer("00000", "00000", "00000", "00000"));
        }
        //TrailerAdapter is provided via Dagger2 and made with AutoFactory
        //By calling ControllerComponent#getAdapterFactory() instead of using ControllerModule#provideAdapterFactory() and @Inject to inject it in this class, because that doesn't work...
        TrailerAdapter movieTrailerAdapter = controllerComponent.getAdapterFactory().createTrailerAdapter(fragmentActivity, movieTrailerList);
        trailersListView.setAdapter(movieTrailerAdapter);
        //Show whole listview regardless of it being in a scrollview...
        int height = Utility.setListViewHeightBasedOnChildren(trailersListView);

        setScrollViewPosition(_TRAILER, height);
    }

    public void addReviews(ListView reviewsListView, List<MovieReview> movieReviewsList) {
        if (movieReviewsList.isEmpty()) {
            //Let user know there are no reviews, by appropriating the list with an empty movie
            movieReviewsList.add(new MovieReview("00000", "00000"));
        }
        //ReviewAdapter is provided via Dagger2 and made with AutoFactory.
        //By calling ControllerComponent#getAdapterFactory() instead of using ControllerModule#provideAdapterFactory() and @Inject to inject it in this class, because that doesn't work...
        ReviewAdapter movieReviewAdapter = controllerComponent.getAdapterFactory().createReviewAdapter(fragmentActivity, movieReviewsList);
        reviewsListView.setAdapter(movieReviewAdapter);
        //Show whole listview regardless of it being in a scrollview...
        int height = Utility.setListViewHeightBasedOnChildren(reviewsListView);

        setScrollViewPosition(_REVIEW, height);
    }

    public void setScrollViewPosition(String movieTrivia, int height) {

        //Only enter when screen was rotated and former mOldScrollPositionArray and mOldScrollViewHeight was saved before
        if (mOldScrollPositionArray != null && mOldScrollViewHeight != 0) {
            if (movieTrivia == _TRAILER) {
                mTrailer = true;
                mHeightOfListViews += height;
            } else if (movieTrivia == _REVIEW) {
                mReview = true;
                mHeightOfListViews += height;
            }
            if (mReview && mTrailer) {
                //Don't collapse when screen was rotated
                AppBarLayout appBar = (AppBarLayout) fragmentActivity.findViewById(R.id.app_bar);
                appBar.setExpanded(false);

                int total = 100; //Total is 100%

                //Convert correctly. Calculate percentage of former proportions
                //(Scrollviewposition / Height ) * 100 = percentage
                double percentage = ((double) mOldScrollPositionArray[1] / (double) mOldScrollViewHeight);
                percentage = percentage * total;

                //Retrieve current height of scrollview
                mScrollView = (NestedScrollView) getActivity().findViewById(R.id.movie_detail_container);
                double newScrollViewHeight = mScrollView.getChildAt(0).getHeight();
                //Add the height of both MovieTrailers- and MovieReviews ListView
                newScrollViewHeight = newScrollViewHeight + mHeightOfListViews;

                //Convert correctly. Calculate percentage to correct position for current proportions
                //(percentage /100)* (current)Height = Scrollviewposition
                newScrollPosition = (percentage / total) * newScrollViewHeight;

                double correction;

                //Add half of screen for correction purposes....
                int orientation = this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    correction = 1.20; //1.20, just because... after lots of trial and error this gives the best experience

                    //Portrait
                    final int viewWidth = mScrollView.getChildAt(0).getWidth();
                    newScrollPosition = newScrollPosition - (viewWidth * correction);
                } else {
                    correction = 5; //5, just because... after lots of trial and error this gives the best experience

                    //Landscape
                    final int viewHeight = mScrollView.getChildAt(0).getHeight();
                    newScrollPosition = newScrollPosition - (viewHeight / correction);
                }

                if (newScrollPosition <= newScrollViewHeight) {

                    mScrollView.post(new Runnable() {
                        public void run() {
                            mScrollView.scrollTo(mOldScrollPositionArray[0], (int) newScrollPosition);
                            mReview = false;
                            mTrailer = false;
                        }
                    });
                }
            }
        }
    }
}
