package com.example.testing.popularmoviestabs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testing.popularmoviestabs.di.component.ControllerComponent;
import com.example.testing.popularmoviestabs.di.component.DaggerControllerComponent;
import com.example.testing.popularmoviestabs.di.module.AdapterModule;
import com.example.testing.popularmoviestabs.di.module.ControllerModule;
import com.example.testing.popularmoviestabs.di.module.NetworkModule;
import com.example.testing.popularmoviestabs.model.MoviesResponse;
import com.example.testing.popularmoviestabs.pojo.Movie;
import com.example.testing.popularmoviestabs.rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.testing.popularmoviestabs.Utility.POPULAR;
import static com.example.testing.popularmoviestabs.Utility.TOPRATED;

/**
 * Represents a tab(3 in total) in MainActivity.
 * Created by Lok on 07/09/2017.
 */
public class MoviesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    //The fragment argument representing the section number for this fragment.
    private static final String ARG_MOVIEMATERIAL = "section_number";
    private static final int POPULAR_ = 1;
    private static final int TOPRATED_ = 2;
    private static final int FAVOURITES_ = 3;
    private static final String KEY_RECYCLER_STATE = "recycler_state";
    private static final String API_KEY = BuildConfig.POPULAR_MOVIES_API_KEY;
    @Inject
    Bundle mBundleRecyclerViewState;
    @Inject
    List<Movie> movieList;
    @Inject
    RecyclerAdapter recyclerAdapter;
    @Inject
    Picasso picasso;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.movie_list_ordinary_textview)
    TextView movie_list_ordinary_textview;
    @Inject
    ApiInterface apiService;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ControllerComponent controllerComponent;
    private CompositeDisposable compositeDisposable;
    private Unbinder unbinder;
    private Utility.MovieContent movieType;

    public MoviesFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MoviesFragment newInstance(int position) {
        MoviesFragment fragment = new MoviesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MOVIEMATERIAL, position);
        fragment.setArguments(args);
        return fragment;
    }

    public ControllerComponent getActivityComponent() {
        if (controllerComponent == null) {
            controllerComponent = DaggerControllerComponent
                    .builder()
                    .controllerModule(new ControllerModule(this))
                    .networkModule(new NetworkModule())
                    .adapterModule(new AdapterModule(this))
                    .build();
        }
        return controllerComponent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.movie_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        //Set up RecyclerView
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        handleMovies();
    }

    @Override
    public void onPause() {
        super.onPause();

        //Save RecyclerView scroll position
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void handleMovies() {
        movie_list_ordinary_textview.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        //First check if user clicked on Favourites, cause if so, skip the other paths to show movielist. (Show favourites from cache regardless of whether there's an internet connection or not)
        //FAVOURITES
        Bundle bundle = getArguments();
        int position = (int) bundle.get(ARG_MOVIEMATERIAL);

        //1 = POPULAR
        //2 = TOPRATED
        //3 = FAVOURITES
        //Retrieve from cache
        if (position == FAVOURITES_) {
            movieType = Utility.MovieContent.FAVOURITE_;
            movieList = getFromCache(); //A1.

            if (movieList == null || movieList.size() == 0) {
                movie_list_ordinary_textview.setText(R.string.no_favourites);
                movie_list_ordinary_textview.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
            } else {
                showMovies(movieList);
            }
        }

        //When there's no internet
        if (!Utility.isNetworkAvailable(getContext())) {
            switch (position) {
                case POPULAR_: {
                    movieType = Utility.MovieContent.POPULAR_;
                    movieList = getFromCache(); //A1.

                    if (movieList == null || movieList.size() == 0) {
                        movie_list_ordinary_textview.setText(R.string.no_movies);//(R.string.no_popular_movies); *As a hotfix, settle for generic "No movies" text, because of an unresolved issue with the way FixedFragmentStatePagerAdapter seems to handle keeping/showing the right Fragment that needs to be visible at that time... Currently it only shows the String resource "no_top_rated_movies" for tab "Popular" and tab "Top Rated"
                        movie_list_ordinary_textview.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.INVISIBLE);
                    } else {
                        showMovies(movieList);
                    }
                }
                case TOPRATED_: {
                    movieType = Utility.MovieContent.TOPRATED_;
                    movieList = getFromCache(); //A1.

                    if (movieList == null || movieList.size() == 0) {
                        movie_list_ordinary_textview.setText(R.string.no_movies);//(R.string.no_top_rated_movies); *As a hotfix, settle for generic "No movies" text, because of an unresolved issue with the way FixedFragmentStatePagerAdapter seems to handle keeping/showing the right Fragment that needs to be visible at that time... Currently it only shows the String resource "no_top_rated_movies" for tab "Popular" and tab "Top Rated"
                        movie_list_ordinary_textview.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.INVISIBLE);
                    } else {
                        showMovies(movieList);
                    }
                }
            }
        }
        //Retrieve from internet
        else {
            //Clicked on Popular
            if (position == POPULAR_) {
                movieType = Utility.MovieContent.POPULAR_;

                movie_list_ordinary_textview.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                //Retrieve movies with RXJava2 and Retrofit2
                getMovies(movieType);
            }
            //Clicked on Top Rated
            else if (position == TOPRATED_) {
                movieType = Utility.MovieContent.TOPRATED_;

                movie_list_ordinary_textview.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                //Retrieve movies with RXJava2 and Retrofit2
                getMovies(movieType);
            }
        }
    }

    public Observable<MoviesResponse> getObservable(Utility.MovieContent movieType) {
        if (movieType == Utility.MovieContent.POPULAR_) {
            return apiService.getPopularMovies(API_KEY);
        } else if (movieType == Utility.MovieContent.TOPRATED_) {
            return apiService.getTopRatedMovies(API_KEY);
        }
        return null;
    }

    public void getMovies(final Utility.MovieContent movieType) {
        Observable<MoviesResponse> observable = getObservable(movieType);

        if (observable != null) {
            //Retrieve popular movies with RetroFit, which is retrieved through an Observable (stream)
            //It's also added to a CompositeDisposable so that resources will be freed in onPause()
            compositeDisposable.add(observable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(new DisposableObserver<MoviesResponse>() {
                        @Override
                        public void onNext(MoviesResponse moviesResponse) {
                            movieList = moviesResponse.getResults();
                        }

                        @Override
                        public void onError(Throwable e) {
                            movieList = null;
                        }

                        @Override
                        public void onComplete() {
                            //Save MovieList to Shared Preferences, for retrieval later when there's no internet and images need to be shown from cache
                            Utility.saveToSharedPref(getContext(), movieList, movieType);

                            //Show movieList
                            showMovies(movieList);
                        }
                    }));
        }
    }

    public List<Movie> getFromCache() {
        //A. Retrieve list from SharedPreferences (Which is used to view images already cached before by PicassoCache in MovieAdapter)
        //Retrieve from Shared Preferences (Popular, Top Rated) and from Database (Favourites)
        //Retrieve list
        Gson gson = new Gson();
        String json = "";
        //Retrieve From SharedPreferences
        MainActivity activity = (MainActivity) getActivity();

        Bundle bundle = getArguments();
        int position = (int) bundle.get(ARG_MOVIEMATERIAL);

        //1 = POPULAR
        //2 = TOPRATED
        //3 = FAVOURITES
        if (position == POPULAR_) {
            //Retrieve POPULAR
            json = Utility.getSharedPref(getContext(), 0, POPULAR, "");
            movieList = gson.fromJson(json, new TypeToken<ArrayList<Movie>>() {
            }.getType());
        } else if (position == TOPRATED_) {
            //Retrieve TOPRATED
            json = Utility.getSharedPref(getContext(), 0, TOPRATED, "");
            movieList = gson.fromJson(json, new TypeToken<ArrayList<Movie>>() {
            }.getType());
        }
        //Retrieve From Database
        else if (position == FAVOURITES_) {
            //Retrieve FAVOURITES
            movieList = Utility.retrieveFavouritesFromDB(getContext());
        }

        if (!Utility.isNetworkAvailable(getContext())) {
            Toast.makeText(getContext(), "There's no internet connection", Toast.LENGTH_SHORT).show();
        }

        return movieList;
    }

    public void showMovies(List<Movie> movieList) {
        //Set up the adapter for the recyclerview
        recyclerAdapter.setMoviesList(movieList);
        recyclerAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(recyclerAdapter);

        // restore RecyclerView scroll position
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    public void refreshList()
    {
        swipeRefreshLayout.setRefreshing(false);
        handleMovies();
    }

    //INNER CLASSES
    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        //FIELDS
        private List<Movie> movies;

        //CONSTRUCTOR
        public RecyclerAdapter() {
        }

        //METHODS
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_content, parent, false);
            return new ViewHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Movie movie = movies.get(position);
            holder.bindPhoto(movie);
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }

        public void setMoviesList(List<Movie> movies) {
            this.movies = movies;
        }

        //ANOTHER NESTED INNER CLASS
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            //FIELDS
            private final String MOVIE = "Movie";
            public Movie movie;
            @BindView(R.id.movie_list_item)
            ImageView movieImage;

            //CONSTRUCTOR
            public ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }

            //METHODS
            public void bindPhoto(Movie movie) {
                this.movie = movie;
                picasso.load("http://image.tmdb.org/t/p/w342/" + movie.getPoster_path()).into(movieImage);
            }

            @OnClick(R.id.movie_list_item)
            public void onClick(View v) {
                //Save RecyclerView scroll position
                mBundleRecyclerViewState = new Bundle();
                Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
                mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);

                //Go to MovieDetailActivity and pass ArrayList of Movies with it
                Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(MOVIE, movieList.get(getAdapterPosition()));
                intent.putExtra(MOVIE, bundle);
                startActivity(intent);
            }
        }
    }
}

