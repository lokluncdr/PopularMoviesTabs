package com.example.testing.popularmoviestabs;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.testing.popularmoviestabs.data.FavouriteColumns;
import com.example.testing.popularmoviestabs.data.FavouriteProvider;
import com.example.testing.popularmoviestabs.pojo.Movie;
import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lok on 25/11/16.
 */

public class Utility {
    //FIELDS
    //For retrieval from Shared Preferences
    public static final String POPULAR = "com.example.testing.popularmoviestabs.PREFERENCE_FILE_KEY_POPULAR";
    public static final String TOPRATED = "com.example.testing.popularmoviestabs.PREFERENCE_FILE_KEY_TOPRATED";
    private static SharedPreferences sharedPreferences;

    //METHODS
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String roundOff(String vote_average) {
        double average = Double.parseDouble(vote_average);
        average = new BigDecimal(average).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
        return String.valueOf(average) + "/10";
    }

    public static int setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return 0;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

        return params.height;
    }

    public static void saveToSharedPref(Context context, List<Movie> movieList, MovieContent movieType) {
        if (movieType == MovieContent.POPULAR_) {
            sharedPreferences = context.getSharedPreferences(POPULAR, 0);
        } else if (movieType == MovieContent.TOPRATED_) {
            sharedPreferences = context.getSharedPreferences(TOPRATED, 0);
        }
        //If a preferences file by this name does not exist, it will be created when you retrieve
        // an editor (SharedPreferences.edit()) and then commit changes (Editor.commit()).
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //Save to Shared Preferences
        Gson gson = new Gson();
        String movieJson = gson.toJson(movieList);

        if (movieType == MovieContent.POPULAR_) {
            editor.putString(POPULAR, movieJson);
        } else if (movieType == MovieContent.TOPRATED_) {
            editor.putString(TOPRATED, movieJson);
        }
        editor.apply();
    }

    public static String getSharedPref(Context context, int mode, String key, String value) {
        sharedPreferences = context.getSharedPreferences(key, mode);
        return sharedPreferences.getString(key, value);
    }

    public static void saveToDatabase(Context context, Movie movie) {
        if (!isInDatabase(context, movie)) {
            //Store values that the ContentResolver can process
            ContentValues values = new ContentValues();
            values.put(FavouriteColumns.FavouriteId, movie.getId());
            values.put(FavouriteColumns.Title, movie.getTitle());
            values.put(FavouriteColumns.ReleaseDate, movie.getRelease_date());
            values.put(FavouriteColumns.PosterPath, movie.getPoster_path());
            values.put(FavouriteColumns.VoteAverage, movie.getVote_average());
            values.put(FavouriteColumns.Overview, movie.getOverview());

            Uri uri = context.getContentResolver().insert(FavouriteProvider.Favourites.CONTENT_URI, values);
            Toast.makeText(context, R.string.saved, Toast.LENGTH_SHORT).show();
        }
    }

    public static void removeFromDatabase(Context context, Movie movie) {
        ContentResolver cr = context.getContentResolver();
        String where = "favouriteId=?";
        String[] args = new String[]{movie.getId()};
        cr.delete(FavouriteProvider.Favourites.CONTENT_URI, where, args);
    }

    public static boolean isInDatabase(Context context, Movie movie) {
        String[] requestedColumns = {
                FavouriteColumns.FavouriteId,
                FavouriteColumns.Title,
                FavouriteColumns.ReleaseDate,
                FavouriteColumns.PosterPath,
                FavouriteColumns.VoteAverage,
                FavouriteColumns.Overview
        };

        Cursor c = context.getContentResolver().query(
                FavouriteProvider.Favourites.CONTENT_URI, //Uri, The URI, using the content:// scheme, for the content to retrieve
                requestedColumns, //projection, pass all columns
                FavouriteColumns.FavouriteId + "='" + movie.getId() + "'",  //selection, A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given URI.
                null, //selectionArgs
                null); //sortOrder

        c.moveToFirst();
        if (c != null) {
            if (c.getCount() == 1) {
                // already inserted
                c.close();
                return true;
            }
        }
        // row does not exist
        return false;
    }

    public static ArrayList<Movie> retrieveFavouritesFromDB(Context context) {
        ArrayList<Movie> movieList = new ArrayList<>();

        String[] requestedColumns = {
                FavouriteColumns.FavouriteId,
                FavouriteColumns.Title,
                FavouriteColumns.ReleaseDate,
                FavouriteColumns.PosterPath,
                FavouriteColumns.VoteAverage,
                FavouriteColumns.Overview
        };

        Cursor c = context.getContentResolver().query(
                FavouriteProvider.Favourites.CONTENT_URI, //Uri, The URI, using the content:// scheme, for the content to retrieve
                requestedColumns, //projection, pass all columns
                null,  //selection, A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given URI.
                null, //selectionArgs
                null); //sortOrder

        c.moveToFirst();
        for (int i = 0; i != c.getCount(); i++) {
            //Convert cursor to Movie object
            Movie movie = populateMovie(c);
            //Add movie to list
            movieList.add(movie);
            c.moveToNext();
        }
        c.close();
        return movieList;
    }

    public static Movie populateMovie(Cursor cursor) {
        Movie m = new Movie();

        try {
            m.setId(cursor.getString(cursor.getColumnIndex(FavouriteColumns.FavouriteId)));
            m.setTitle(cursor.getString(cursor.getColumnIndex(FavouriteColumns.Title)));
            m.setRelease_date(cursor.getString(cursor.getColumnIndex(FavouriteColumns.ReleaseDate)));
            m.setPoster_path(cursor.getString(cursor.getColumnIndex(FavouriteColumns.PosterPath)));
            m.setVote_average(cursor.getString(cursor.getColumnIndex(FavouriteColumns.VoteAverage)));
            m.setOverview(cursor.getString(cursor.getColumnIndex(FavouriteColumns.Overview)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m;
    }

    public enum MovieContent {
        POPULAR_, TOPRATED_, FAVOURITE_, TRAILER_, REVIEW_
    }
}
