package com.example.testing.popularmoviestabs.adapters;

import android.content.Context;

import com.example.testing.popularmoviestabs.pojo.MovieReview;
import com.example.testing.popularmoviestabs.pojo.MovieTrailer;
import com.google.auto.factory.AutoFactory;

import java.util.List;

/**
 * Created by Lok on 18/12/2017.
 */
@AutoFactory
public final class AdapterFactory {

    public AdapterFactory() {
    }

    public TrailerAdapter createTrailerAdapter(Context context, List<MovieTrailer> movieTrailerList) {
        return new TrailerAdapter(context, movieTrailerList);
    }

    public ReviewAdapter createReviewAdapter(Context context, List<MovieReview> movieReviewList) {
        return new ReviewAdapter(context, movieReviewList);
    }
}
