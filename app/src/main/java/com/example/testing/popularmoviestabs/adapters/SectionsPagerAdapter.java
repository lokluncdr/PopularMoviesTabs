package com.example.testing.popularmoviestabs.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.example.testing.popularmoviestabs.MoviesFragment;
import com.example.testing.popularmoviestabs.R;

import java.util.List;

/**
 * Created by Lok on 22/11/2017.
 */

/***************************************************************************
 * Takes care of making the Fragments and switching between them together  *
 * with ViewPager and TabLayout instances in onCreate()                    *
 * A FragmentStatePagerAdapter that returns a fragment corresponding to    *
 * one of the sections/tabs/pages.                                         *
 * 0 = Popular                                                             *
 * 1 = Top Rated                                                           *
 * 2 = Favourites                                                          *
 ***************************************************************************/
public class SectionsPagerAdapter extends FixedFragmentStatePagerAdapter {

    private Context context;
    private String[] labels = new String[]{
            "0",
            "1",
            "2"
    };
    private List<MoviesFragment> moviesFragments;

    public SectionsPagerAdapter(Context context, FragmentManager fm, List<MoviesFragment> moviesFragments) {
        super(fm);
        this.context = context;
        this.moviesFragments = moviesFragments;
    }

    @Override
    public String getTag(int position) {
        return labels[position];
    }

    @Override
    // getItem is called to instantiate the fragment for the given page.
    public Fragment getItem(int position) {
        //Set up MoviesFragment
        return moviesFragments.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.title_popular);
            case 1:
                return context.getString(R.string.title_top_rated);
            case 2:
                return context.getString(R.string.title_favourites);
        }
        return null;
    }
}
