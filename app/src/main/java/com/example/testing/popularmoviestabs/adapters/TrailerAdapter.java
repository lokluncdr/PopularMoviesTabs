package com.example.testing.popularmoviestabs.adapters;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testing.popularmoviestabs.R;
import com.example.testing.popularmoviestabs.pojo.MovieTrailer;

import java.util.List;

/**
 * Created by Lok on 25/11/16.
 */
public class TrailerAdapter extends ArrayAdapter<MovieTrailer> implements View.OnClickListener {
    //FIELDS
    private final String NOTRAILER = "00000";
    private Context context;
    private List<MovieTrailer> movieTrailers;

    /**
     * Constructor
     *
     * @param context The current context.
     */
    public TrailerAdapter(Context context, List<MovieTrailer> movieTrailers) {
        super(context, 0, movieTrailers);
        this.context = context;
        this.movieTrailers = movieTrailers;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater = null;

        //Gets the MovieTrailer object from the ArrayAdapter at the appropriate position
        MovieTrailer movieTrailer = movieTrailers.get(position);

        if (convertView == null) {
            //getCustomApplication the inflater and inflate the XML layout for each item
            inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_detail_trailer, null);

            // well set up the ViewHolder
            viewHolder = new ViewHolder();
            viewHolder.textViewTrailer = (TextView) convertView.findViewById(R.id.movie_detail_trailer);

            // store the holder with the view.
            convertView.setTag(movieTrailer);
        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (movieTrailer.getKey() != NOTRAILER && movieTrailer.getId() != NOTRAILER
                && movieTrailer.getName() != NOTRAILER && movieTrailer.getType() != NOTRAILER) {
            viewHolder.textViewTrailer.setText(movieTrailer.getType());

            //Set OnClicklistener
            convertView.setOnClickListener(this);
        } else {
            convertView = inflater.inflate(R.layout.ordinary_textview, null);
            //Set up the ViewHolder
            viewHolder.textViewNoTrailers = (TextView) convertView.findViewById(R.id.movie_detail_ordinary_textview);
            viewHolder.textViewNoTrailers.setText(R.string.no_trailers);
        }

        return convertView;
    }

    public void onClick(View v) {
        //Show that user clicked on item
        Animation animation = new AlphaAnimation(0.3f, 1.0f);
        animation.setDuration(400);
        v.startAnimation(animation);

        MovieTrailer movieTrailer = (MovieTrailer) v.getTag();
        watchYoutubeVideo(movieTrailer.getKey());
    }

    public void watchYoutubeVideo(String key) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + key));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + key));
        try {
            context.startActivity(appIntent);
            Toast.makeText(getContext(), R.string.open_youtube_app, Toast.LENGTH_SHORT).show();
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
            Toast.makeText(getContext(), R.string.open_in_browser, Toast.LENGTH_SHORT).show();
        }
    }

    //Viewholder pattern for reusing views by holding references to the Id's of the views
    public static class ViewHolder { //Can't seem to use ButterKnife, because of 2 different inflated View resources being used (depending on if there are trailers)
        public TextView textViewTrailer;
        public TextView textViewNoTrailers;
    }
}
