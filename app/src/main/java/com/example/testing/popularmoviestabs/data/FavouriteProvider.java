package com.example.testing.popularmoviestabs.data;

import android.net.Uri;

import net.simonvt.schematic.annotation.ContentProvider;
import net.simonvt.schematic.annotation.ContentUri;
import net.simonvt.schematic.annotation.InexactContentUri;
import net.simonvt.schematic.annotation.TableEndpoint;

/**
 * Created by Lok on 18/01/17.
 */

@ContentProvider(
        authority = FavouriteProvider.AUTHORITY,
        database = FavouriteDatabase.class
)
public class FavouriteProvider {
    //FIELDS
    public static final String AUTHORITY = "com.example.testing.popularmoviestabs.data.FavouriteProvider";
    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    private static Uri buildUri(String... paths) {
        Uri.Builder builder = BASE_CONTENT_URI.buildUpon();
        for (String path : paths) {
            builder.appendPath(path);
        }
        return builder.build();
    }

    //INTERFACE
    interface Path {
        String FAVOURITES = "Favourites";
    }

    //INNER CLASS
    @TableEndpoint(table = FavouriteDatabase.FAVOURITES)
    public static class Favourites {
        @ContentUri(
                path = Path.FAVOURITES,
                type = "vnd.android.cursor.dir/favourites" //return all our rows from our tables
        )
        public static final Uri CONTENT_URI = buildUri(Path.FAVOURITES);

        @InexactContentUri(
                name = "FAVOURITE_ID",
                path = Path.FAVOURITES + "/#",
                type = "vnd.android.cursor.sectionPosition/favourite",  // return a single row based on an ID
                whereColumn = FavouriteColumns._ID,
                pathSegment = 1
        )
        public static Uri withId(int id) {
            return buildUri(Path.FAVOURITES, String.valueOf(id));
        }
    }
}
