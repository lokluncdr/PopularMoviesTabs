package com.example.testing.popularmoviestabs.di.component;

import com.example.testing.popularmoviestabs.MainActivity;
import com.example.testing.popularmoviestabs.MovieDetailActivity;
import com.example.testing.popularmoviestabs.MovieDetailFragment;
import com.example.testing.popularmoviestabs.MoviesFragment;
import com.example.testing.popularmoviestabs.adapters.AdapterFactory;
import com.example.testing.popularmoviestabs.di.PerActivity;
import com.example.testing.popularmoviestabs.di.PerFragment;
import com.example.testing.popularmoviestabs.di.module.AdapterModule;
import com.example.testing.popularmoviestabs.di.module.ControllerModule;
import com.example.testing.popularmoviestabs.di.module.NetworkModule;

import dagger.Component;

/**
 * Created by Lok on 22/11/2017.
 */
@PerActivity
@PerFragment
@Component(modules = {ControllerModule.class, NetworkModule.class, AdapterModule.class})
public interface ControllerComponent {
    void inject(MainActivity mainActivity);

    void inject(MoviesFragment moviesFragment);

    void inject(MovieDetailActivity movieDetailActivity);

    void inject(MovieDetailFragment movieDetailFragment);

    AdapterFactory getAdapterFactory();
}
