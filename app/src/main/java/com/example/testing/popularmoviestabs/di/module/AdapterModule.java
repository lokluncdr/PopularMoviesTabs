package com.example.testing.popularmoviestabs.di.module;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.example.testing.popularmoviestabs.MoviesFragment;
import com.example.testing.popularmoviestabs.adapters.AdapterFactory;
import com.example.testing.popularmoviestabs.adapters.SectionsPagerAdapter;
import com.example.testing.popularmoviestabs.di.ActivityContext;
import com.example.testing.popularmoviestabs.di.PerActivity;
import com.example.testing.popularmoviestabs.di.PerFragment;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lok on 19/12/2017.
 */
@Module
public class AdapterModule {
    private FragmentActivity fragmentActivity;
    private ArrayList<MoviesFragment> moviesFragments;
    private MoviesFragment moviesFragment;

    public AdapterModule() {
    }

    public AdapterModule(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
    }

    public AdapterModule(MoviesFragment moviesFragment) {
        this.moviesFragment = moviesFragment;
    }

    //////////////////////////////
    //  MainActivity            //
    //////////////////////////////
    @Provides
    @ActivityContext
    Context provideContext() {
        return fragmentActivity;
    }

    @Provides
    @PerActivity
    FragmentManager provideFragmentManager() {
        return fragmentActivity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    ArrayList<MoviesFragment> provideMoviesFragments() {
        moviesFragments = new ArrayList<MoviesFragment>();
        moviesFragments.add(MoviesFragment.newInstance(1));
        moviesFragments.add(MoviesFragment.newInstance(2));
        moviesFragments.add(MoviesFragment.newInstance(3));

        return moviesFragments;
    }

    @Provides
    @PerActivity
    SectionsPagerAdapter provideSectionsPagerAdapter(@ActivityContext Context context, FragmentManager fragmentManager, ArrayList<MoviesFragment> moviesFragments) {
        return new SectionsPagerAdapter(context, fragmentManager, moviesFragments);
    }

    //////////////////////////////
    //  MoviesFragment          //
    //////////////////////////////
    @Provides
    @PerFragment
    MoviesFragment.RecyclerAdapter provideRecyclerAdapter() {
        MoviesFragment.RecyclerAdapter recyclerAdapter = moviesFragment.new RecyclerAdapter();
        return recyclerAdapter;
    }

    //////////////////////////////
    //  MovieDetailFragment     //
    //////////////////////////////
    @Provides
    @PerFragment
    AdapterFactory provideAdapterFactory() {
        return new AdapterFactory();
    }
}
