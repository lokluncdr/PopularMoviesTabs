package com.example.testing.popularmoviestabs.di.module;

import android.os.Bundle;

import com.example.testing.popularmoviestabs.MovieDetailFragment;
import com.example.testing.popularmoviestabs.MoviesFragment;
import com.example.testing.popularmoviestabs.di.PerActivity;
import com.example.testing.popularmoviestabs.di.PerFragment;
import com.example.testing.popularmoviestabs.pojo.Movie;
import com.example.testing.popularmoviestabs.pojo.MovieReview;
import com.example.testing.popularmoviestabs.pojo.MovieTrailer;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lok on 22/11/2017.
 */
@Module
public class ControllerModule {
    private MoviesFragment moviesFragment;

    public ControllerModule() {
    }

    public ControllerModule(MoviesFragment moviesFragment) {
        this.moviesFragment = moviesFragment;
    }

    //////////////////////////////
    //  MoviesFragment          //
    //////////////////////////////
    @Provides
    @PerFragment
    Bundle provideBundle() { //Also for MovieDetailFragment
        return new Bundle();
    }

    @Provides
    @PerFragment
    List<Movie> provideMovieList() {
        return new ArrayList<>();
    }

    @Provides
    @PerFragment
    Picasso providePicasso() {
        Downloader downloader = new OkHttpDownloader(moviesFragment.getContext(), Integer.MAX_VALUE);
        Picasso.Builder builder = new Picasso.Builder(moviesFragment.getContext());
        builder.downloader(downloader);

        return builder.build();
    }

    //////////////////////////////
    //  MovieDetailActivity     //
    //////////////////////////////
    @Provides
    @PerActivity
    MovieDetailFragment provideMovieDetailFragment() {
        return new MovieDetailFragment();
    }

    //////////////////////////////
    //  MovieDetailFragment     //
    //////////////////////////////
    @Provides
    @PerFragment
    List<MovieTrailer> provideMovieTrailerList() {
        return new ArrayList<>();
    }

    @Provides
    @PerFragment
    List<MovieReview> provideMovieReviewList() {
        return new ArrayList<>();
    }
}
