package com.example.testing.popularmoviestabs.di.module;

import com.example.testing.popularmoviestabs.di.PerFragment;
import com.example.testing.popularmoviestabs.rest.ApiInterface;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lok on 19/12/2017.
 */
@Module
public class NetworkModule {
    private String BASE_URL = "http://api.themoviedb.org/3/";

    public NetworkModule()
    {
    }

    //////////////////////////////////////////
    // MoviesFragment + MovieDetailFragment //
    //////////////////////////////////////////
    @Provides
    @PerFragment
    Retrofit provideRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    @PerFragment
    ApiInterface provideApiInterface(Retrofit retrofit) {
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        return apiInterface;
    }
}
