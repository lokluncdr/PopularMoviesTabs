package com.example.testing.popularmoviestabs.model;

import com.example.testing.popularmoviestabs.pojo.MovieTrailer;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lok on 24/10/2017.
 */

public class MovieTrailersResponse {
    @SerializedName("id")
    private int id;
    @SerializedName("results")
    private List<MovieTrailer> results;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<MovieTrailer> getResults() {
        return results;
    }

    public void setResults(List<MovieTrailer> results) {
        this.results = results;
    }
}
