package com.example.testing.popularmoviestabs.pojo;

/**
 * Created by Lok on 24/11/16.
 */
public class MovieTrailer {

    //FIELDS
    private String id;
    private String key;
    private String name;
    private String type;

    //CONSTRUCTOR
    public MovieTrailer(String id, String key, String name, String type) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.type = type;
    }

    //METHODS
    @Override
    public String toString() {
        return "MovieTrailer{" +
                "id='" + id + '\'' +
                ",key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
