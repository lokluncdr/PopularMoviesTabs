package com.example.testing.popularmoviestabs.rest;

import com.example.testing.popularmoviestabs.model.MovieReviewsResponse;
import com.example.testing.popularmoviestabs.model.MovieTrailersResponse;
import com.example.testing.popularmoviestabs.model.MoviesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Lok on 19/10/2017.
 *
 */

public interface ApiInterface {
    // Construct the URL for the Popular Movies query
    // ->http://api.themoviedb.org/3/movie/popular?api_key=##############
    @GET("movie/popular")
    Observable<MoviesResponse> getPopularMovies(@Query("api_key") String apiKey);

    // Construct the URL for the Top Rated Movies query
    // ->http://api.themoviedb.org/3/movie/top_rated?api_key=##############
    @GET("movie/top_rated")
    Observable<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    // Construct the URL for the MovieTrailers query
    // ->https://api.themoviedb.org/3/movie/284052/videos?api_key=##############&language=en-US
    @GET("movie/{movieId}/videos")
    Observable<MovieTrailersResponse> getMovieTrailers(@Path("movieId") String movieId, @Query("api_key") String apiKey);

    // Construct the URL for the MovieReviews query
    // ->https://api.themoviedb.org/3/movie/259316/reviews?api_key=##############&language=en-US
    @GET("movie/{movieId}/reviews")
    Observable<MovieReviewsResponse> getMovieReviews(@Path("movieId") String movieId, @Query("api_key") String apiKey);
}
